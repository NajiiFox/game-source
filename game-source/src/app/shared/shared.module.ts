import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/menu.component';
import { MaterialModule } from '../material/material.module';
import { MatInputModule } from '@angular/material';
import { MenuComponent } from './components/menu/menu.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchBarComponent } from './components/searchbar/searchbar';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        MatInputModule,
        ReactiveFormsModule
    ],
    declarations: [
        FooterComponent,
        MenuComponent,
        SearchBarComponent
    ],
    exports: [
        FooterComponent,
        MenuComponent,
        SearchBarComponent
    ]
})
export class SharedModule { }
