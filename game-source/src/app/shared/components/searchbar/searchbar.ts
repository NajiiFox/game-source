import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
    selector: 'app-search-bar',
    templateUrl: './searchbar.html',
    styleUrls: ['./searchbar.scss']
})
export class SearchBarComponent {

    @Output() pesquisar: EventEmitter<string>;
    oForm: FormGroup;
    sValor: string;

    constructor(
        oFormBuilder: FormBuilder
    ) {
        this.oForm = oFormBuilder.group({
            edtFomPesquisa: ['', [Validators.minLength(3)]],
        });
        this.pesquisar = new EventEmitter<string>();
    }

    onPesquisar() {
        this.sValor = this.edtFomPesquisa.value;
        this.pesquisar.emit(this.sValor);
    }

    get edtFomPesquisa() {
        return this.oForm.get('edtFomPesquisa');
    }
}
